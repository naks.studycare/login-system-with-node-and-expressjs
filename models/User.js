const mongoose = require('mongoose')
const schema = mongoose.Schema

const userSchema = new schema({
   email: {
            type: String,
            minlength: 5,
            trim: true,
            unique: true,
            required: true
    },

    password: {
        type: String,
        minlength: 6,
        required: true
    }
})

module.exports = mongoose.model('users', userSchema)