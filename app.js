const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const bcrypt = require('bcryptjs')

const user = require('./models/User')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

//*******connected to database ***********************
mongoose.connect('mongodb://127.0.0.1/login',()=>{
    console.log('connected to mongoose')
})


//  post request for register ================

app.post('/register', (req,res)=>{
    const newUser = new user()

    newUser.email = req.body.email
    newUser.password = req.body.password

    bcrypt.genSalt(10,(err, salt)=>{
        bcrypt.hash(newUser.password,salt,(err,hash)=>{
            if(err) return err
            newUser.password = hash

            newUser.save().then(saveddata=>{
                res.send('data is saved')
            }).catch(err=>{
                res.send(`an error occurs because of ....... ${err}`)
            })
        })
    }) 
})


// post request for login *******************=================

app.post('/login',(req,res)=>{
    user.findOne({email: req.body.email}).then(user=>{
        if(user){
            bcrypt.compare(req.body.password,user.password,(err, matched)=>{
                if(err) return err
                
                if(matched){
                    res.send('you are able to login')
                }

                else{
                    res.send('incorrect password')
                }
            })
        }

        else{
            res.send('email address not matched')
        }
    })
})

// * **  *****  port setup ************************
let port = 4111 || process.env.PORT
app.listen(port, ()=> console.log(`listening on port ${port}`))